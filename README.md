### `truffle migrate`

Build your smart contract.

### `truffle develop`

Run the smart contract in the development mode.

### `migrate --reset`
Run migration and reset cached data.

### `cd client`
### `yarn start`

Run the frontend app in the development mode

### `truffle test`

Test smart contracts with truffle