import { useEffect, useState } from 'react';
import { Header } from './Header';
import { NewTransfer } from './NewTransfer';
import { Transfers } from './Transfers';
import { getWeb3, getWallet } from './utils';

function App() {
  const [web3, setWeb3] = useState(undefined);
  const [accounts, setAccounts] = useState(undefined);
  const [wallet, setWallet] = useState(undefined);
  const [approvers, setApprovers] = useState([]);
  const [quorum, setQuorum] = useState(0)
  const [transfers, setTransfers] = useState([]);

  useEffect(() => {
    const init = async () => {
      const web3 = await getWeb3();
      const accounts = await web3.eth.getAccounts();
      const wallet = await getWallet(web3);
      const approvers = await wallet.methods.getApprovers().call();
      const quorum = await wallet.methods.quorum().call();
      const transfers = await wallet.methods.getTransfers().call();

      setWeb3(web3);
      setAccounts(accounts);
      setWallet(wallet);
      setApprovers(approvers);
      setQuorum(quorum);
      setTransfers(transfers);
    }
    init();
  }, []);

  const createTransfer = (transfer) => {
    return wallet.methods.createTransfer(transfer.amount, transfer.to).send({ from: accounts[0] })
  }

  const approveTransfer = (id) => {
    return wallet.methods.approveTransfer(id).send({ from: accounts[0] })
  }

  if (typeof web3 === 'undefined' || typeof accounts === 'undefined' || typeof wallet === 'undefined' || approvers.length === 0 || quorum === 0) {
    return <div>Loading...</div>
  }

  return (
    <div className="App">
      Multisig Dapp
      <Header approvers={approvers} quorum={quorum}></Header>
      <NewTransfer createTransfer={createTransfer} />
      <Transfers transfers={transfers} approveTransfer={approveTransfer} />
    </div>
  );
}

export default App;
