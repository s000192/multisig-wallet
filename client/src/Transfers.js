export const Transfers = ({ transfers, approveTransfer }) => {
    return (
        <div>
            <h2>List of transfer</h2>
            <ol>
                {transfers.map((transfer) => {
                    return (
                        <>
                            <li>
                                <ul>
                                    <li>ID: {transfer.id}</li>
                                    <li>Amount: {transfer.amount}</li>
                                    <li>To: {transfer.to}</li>
                                    <li>Approvals: {transfer.approvals}</li>
                                    <li>Sent: {transfer.sent ? "Yes" : "No"}</li>
                                </ul>
                            </li>
                            <button onClick={() => { approveTransfer(transfer.id) }}>Approve</button>
                        </>
                    )
                })}
            </ol>
        </div>
    )
}